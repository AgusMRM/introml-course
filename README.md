# Introduction to Machine Learning

This repository contains materials from the "Introduction to Machine Learning" course offered by the National University of Rosario. The course covers theoretical fundamentals of machine learning algorithms and practical implementations using Python, scikit-learn, Keras, focusing on Decision Trees, Convolutional Neural Networks (CNN), Artificial Neural Networks (ANN), K-Nearest Neighbors (KNN), and Bayes methods.

## Course Overview

The course covers the following topics:

- Theoretical Fundamentals of Machine Learning Algorithms
- Practical Implementation in Python
- Utilizing scikit-learn for Machine Learning Algorithms
- Introduction to Keras for Deep Learning
- Hands-on Practice with Decision Trees, CNNs, ANNs, KNN, and Bayes methods

## Course Structure

- **Lectures**: Theoretical concepts and algorithm explanations.
- **Practical Sessions**: Hands-on coding sessions using Python and relevant libraries.
- **Assignments**: Assignments to reinforce learning and apply concepts to real-world datasets.

## Contents

- `trees/`: Materials related to Decision Trees.
- `CNN/`: Materials related to Convolutional Neural Networks.
- `ANN/`: Materials related to Artificial Neural Networks.
- `KNN/`: Materials related to K-Nearest Neighbors.
- `BAYES/`: Materials related to Bayes methods.
